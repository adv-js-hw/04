sendRequest("GET", "https://ajax.test-danit.com/api/swapi/films");

function sendRequest(type, url) {
  const xhr = new XMLHttpRequest();
  xhr.open(type, url);
  xhr.send();
  xhr.onload = function () {
    if (xhr.status === 200) {
      const films = JSON.parse(xhr.responseText);

      displayFilms(films);
    } else {
      console.error("Помилка при отриманні даних:", xhr.statusText);
    }
  };
}

async function characters(films) {
  const charactersPromises = films.characters.map(async (characterUrl) => {
    const characterResponse = await fetch(characterUrl);
    const characterData = await characterResponse.json();
    return characterData;
  });

  const characters = await Promise.all(charactersPromises);

  const names = characters.map((character) => character.name);
  console.log(names);

  return names;
}

function displayFilms(movies) {
  const container = document.createElement("div");

  movies.forEach(async (film) => {
    const wrapper = document.createElement("div");

    const title = document.createElement("h2");
    title.textContent = `Episode ${film.episodeId}: ${film.name}`;

    const crawl = document.createElement("p");
    crawl.textContent = `${film.openingCrawl}`;

    const charactersNames = await characters(film);
    const charactersList = document.createElement("ol");
    
    charactersNames.forEach((characterName) => {
      const characterItem = document.createElement("li");
      characterItem.textContent = characterName;
      charactersList.append(characterItem);
    });

    const details = document.createElement("details");
    details.open = true;

    const summary = document.createElement("summary");
    summary.textContent = "Characters:"
    details.append(summary);
    details.append(charactersList);

    wrapper.append(title);
    wrapper.append(crawl);
    wrapper.append(details);
    container.append(wrapper);
  });

  document.body.append(container);
}